    // PopUp Form and thank you popup after sending message
    var $popOverlay = $(".overlay");
    var $popWindow = $(".form");
    var $popSend = $(".button_form")
    var $popClose = $(".form__close")
    var $popOpen = $(".button");

    $(function() {
      // Close Pop-Up after clicking on the button "Close"
      $popOpen.on("click", function() {
        $popOverlay.fadeIn();
        $popWindow.fadeIn();
      });

    //   Close Pop-Up after clicking on the Overlay
    $($popOverlay).on("click", function() {
        $popOverlay.fadeOut();
        $popWindow.fadeOut();
      });
      $($popClose).on("click", function() {
        $popOverlay.fadeOut();
        $popWindow.fadeOut();
      });
      $($popSend).on("click", function() {
        $popOverlay.fadeOut();
        $popWindow.fadeOut();
      });
    })


$(document).ready(function () {

    // $(".button").click(function () {
    //     $(".form_visible").addClass("active");
    // });

    // $(".form__close").click(function () {
    //     $(".form_visible").removeClass("active");
    // });





    var btn = $(".language");
    var list = $(".language__list");

    $(btn).click(function (e) {
        $(list).slideDown(300);
    });$(list).fadeOut();

    jQuery(function($){
        $(document).mouseup(function (e){ // отслеживаем событие клика по веб-документу
            var block = $(".language__list");// определяем элемент, к которому будем применять условия (можем указывать ID, класс либо любой другой идентификатор элемента)
            if (!block.is(e.target) // проверка условия если клик был не по нашему блоку
                && block.has(e.target).length === 0) { // проверка условия если клик не по его дочерним элементам
                block.hide(); // если условия выполняются - скрываем наш элемент
            }
    });
});

$(".faq__btn").click(function () { // задаем функцию при нажатиии на элемент с классом slide-toggle
    $(".faq__hidden-items").slideToggle(300); // плавно скрываем, или отображаем все элементы <div>
});

$('.burger').click(function () {
    $('.nav-menu').toggleClass('activeL');
    $('.burger').toggleClass('_active');
    $('.html').toggleClass('over');
});
    });

/*Collapse*/


var acc = document.getElementsByClassName("faq__item-title");
var i;

for (i = 0; i < acc.length; i++) {
    acc[i].addEventListener("click", function () {
        this.classList.toggle("display");
        var panel = this.nextElementSibling;
        if (panel.style.maxHeight) {
            panel.style.maxHeight = null;
        } else {
            panel.style.maxHeight = panel.scrollHeight + "px";
        }
    })
}
